<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/home', 'IndexController@home');    
Route::get('/register', 'AuthController@register');
Route::post('/welcome2', 'AuthController@send');

route::get('/data-table', function(){
    return view('table.datatable');
});

route::get('/table', function(){
    return view('table.table');
});

//CRUD cast
route::get('/cast/create', 'castController@create');
route::post('/cast', 'castController@store');
route::get('/cast', 'castController@index');
route::get('/cast/{cast_id}', 'castController@show');
route::get('/cast/{cast_id}/edit', 'castController@edit');
route::put('/cast/{cast_id}', 'castController@update');
route::delete('/cast/{cast_id}', 'castController@destroy');