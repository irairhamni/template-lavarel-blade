@extends('layout.master')

@section('judul')
Buat Account Baru!
@endsection

@section(('content'))
    
    <h3>Sign Up form</h3>
    <form action="/welcome2" method="post">
        @csrf
        <label>First Name: </label> <br><br>
        <input type="text" name="nama1"> <br><br>
        <label>Last Name: </label> <br><br>
        <input type="text" name="nama2"> <br><br>
        <label>Gender:</label><br><br>
            <input type="radio" name="Gender" value="Male"> Male <br>
            <input type="radio" name="Gender" value="Female"> Female <br>
            <input type="radio" name="Gender" value="Other"> Other <br>
        <br>
        <label>
            Nationality : <br><br>
            <select>
                <option>Indonesia</option>
                <option>Amerika</option>
                <option>Ingris</option>
            </select>
        </label><br><br>
        Languange Spoken: <br><br>
        <label>
            <input type="checkbox"> Bahasa indonesia
        </label><br>
        <label>
            <input type="checkbox"> English
        </label><br>
        <label>
            <input type="checkbox"> Other
        </label><br>
        <br>
        <label>Bio : <br><br>
            <textarea cols="30" rows="10"></textarea>
        </label> 
        <br>
        <br>
        <input type="submit" value="Sign Up">
    </form>
    @endsection
